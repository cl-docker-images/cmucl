#+TITLE: CMUCL Docker Images

This project contains Docker images to build CMUCL and the infrastructure to
build the images.

This repository is mirrored between both
[[https://gitlab.common-lisp.net/cl-docker-images/cmucl]] and
[[https://github.com/cl-docker-images/cmucl]].

* Unofficial images

  Currently, all images defined on the =master= branch are built and pushed to
  =daewok/cmucl= and =clfoundation/cmucl= on Docker Hub.

* Releasing a new version

  When a new version of CMUCL is released, perform the following steps:

  1. Run the following command to generate the Dockerfiles:

     #+begin_src shell
       ./update.sh $CMUCL_VERSION_NUMBER
     #+end_src

  2. Remove all folders for CMUCL versions that are no longer
     supported. Reminder: the corresponding tags are *not* removed from
     Dockerhub, they will just no longer be automatically built.

  3. Update the version aliases as necessary in
     [[file:generate-stackbrew-library.sh]].

  4. Open a merge request on
     [[https://gitlab.common-lisp.net/cl-docker-images/cmucl]] (preferred) or
     [[https://github.com/cl-docker-images/cmucl]].
