#!/bin/sh

# If the first arg starts with a hyphen, prepend cmucl to arguments.
if [ "${1#-}" != "$1" ]; then
	set -- cmucl "$@"
fi

exec "$@"
