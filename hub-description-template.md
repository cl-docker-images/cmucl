- [Supported Tags](#org761635b)
  - [Simple Tags](#org9e1ea36)
  - [Shared Tags](#org96abf4f)
- [Quick Reference](#org4ace6cb)
- [What is CMUCL?](#orgbe3cacf)
- [How to use this image](#org646e382)
  - [Create a `Dockerfile` in your CMUCL project](#orgef09b04)
  - [Run a single Common Lisp script](#orgb612e5f)
  - [Developing using SLIME](#orgb63e922)
- [What's in the image?](#org3794e20)
- [Image variants](#org2081432)
  - [`%%IMAGE%%:<version>`](#org29d4c99)
  - [`%%IMAGE%%:<version>-slim`](#orgb7d492c)
- [License](#org4cb38f4)



<a id="org761635b"></a>

# Supported Tags


<a id="org9e1ea36"></a>

## Simple Tags

INSERT-SIMPLE-TAGS


<a id="org96abf4f"></a>

## Shared Tags

INSERT-SHARED-TAGS


<a id="org4ace6cb"></a>

# Quick Reference

-   **CMUCL Home Page:** [http://cmucl.org](http://cmucl.org)
-   **Where to file Docker image related issues:** <https://gitlab.common-lisp.net/cl-docker-images/cmucl>
-   **Where to file issues for CMUCL itself:** <https://gitlab.common-lisp.net/cmucl/cmucl/>
-   **Maintained by:** [CL Docker Images Project](https://common-lisp.net/project/cl-docker-images)
-   **Supported platforms:** `linux/amd64`


<a id="orgbe3cacf"></a>

# What is CMUCL?

From [CMUCL's Home Page](http://cmucl.org)

> CMUCL is a free implementation of the Common Lisp programming language which runs on most major Unix platforms. It mainly conforms to the ANSI Common Lisp standard. Here is a summary of its main features:
> 
> -   Support for static arrays that are never moved by GC but are properly removed when no longer referenced.
> 
> -   Unicode support, including many of the most common external formats such as UTF-8 and support for handling Unix, DOS, and Mac end-of-line schemes.
> -   native double-double floats including complex double-double floats and specialized arrays for double-double floats and and complex double-double floats that give approximately 106 bits (32 digits) of precision.
> -   a sophisticated native-code compiler which is capable of powerful type inferences, and generates code competitive in speed with C compilers.
> -   generational garbage collection and multiprocessing capability on the x86 ports.
> -   a foreign function interface which allows interfacing with C code and system libraries, including shared libraries on most platforms, and direct access to Unix system calls.
> -   support for interprocess communication and remote procedure calls.
> -   an implementation of CLOS, the Common Lisp Object System, which includes multimethods and a metaobject protocol.
> -   a graphical source-level debugger using a Motif interface, and a code profiler.
> -   an interface to the X11 Window System (CLX), and a sophisticated graphical widget library (Garnet).
> -   programmer-extensible input and output streams.
> -   an Emacs-like editor implemented in Common Lisp.
> -   freely redistributable: free, with full source code (most of which is in the public domain) and no strings attached (and no warranty). Like the GNU/Linux and \*BSD operating systems, CMUCL is maintained and improved by a team of volunteers collaborating over the Internet.


<a id="org646e382"></a>

# How to use this image


<a id="orgef09b04"></a>

## Create a `Dockerfile` in your CMUCL project

```dockerfile
FROM %%IMAGE%%:latest
COPY . /usr/src/app
WORKDIR /usr/src/app
CMD [ "cmucl", "-load", "./your-daemon-or-script.lisp" ]
```

You can then build and run the Docker image:

```console
$ docker build -t my-cmucl-app
$ docker run -it --rm --name my-running-app my-cmucl-app
```


<a id="orgb612e5f"></a>

## Run a single Common Lisp script

For many simple, single file projects, you may find it inconvenient to write a complete `Dockerfile`. In such cases, you can run a Lisp script by using the CMUCL Docker image directly:

```console
$ docker run -it --rm --name my-running-script -v "$PWD":/usr/src/app -w /usr/src/app %%IMAGE%%:latest cmucl -load ./your-daemon-or-script.lisp
```


<a id="orgb63e922"></a>

## Developing using SLIME

[SLIME](https://common-lisp.net/project/slime/) provides a convenient and fun environment for hacking on Common Lisp. To develop using SLIME, first start the Swank server in a container:

```console
$ docker run -it --rm --name cmucl-slime -p 127.0.0.1:4005:4005 -v /path/to/slime:/usr/src/slime -v "$PWD":/usr/src/app -w /usr/src/app %%IMAGE%%:latest cmucl -load /usr/src/slime/swank-loader.lisp -eval '(swank-loader:init)' -eval '(swank:create-server :dont-close t :interface "0.0.0.0")'
```

Then, in an Emacs instance with slime loaded, type:

```emacs
M-x slime-connect RET RET RET
```


<a id="org3794e20"></a>

# What's in the image?

This image contains CMUCL binaries provided by the CMUCL devs.


<a id="org2081432"></a>

# Image variants

This image comes in several variants, each designed for a specific use case.


<a id="org29d4c99"></a>

## `%%IMAGE%%:<version>`

This is the defacto image. If you are unsure about what your needs are, you probably want to use this one. It is designed to be used both as a throw away container (mount your source code and start the container to start your app), as well as the base to build other images off of.

Some of these tags may have names like buster or stretch in them. These are the suite code names for releases of Debian and indicate which release the image is based on. If your image needs to install any additional packages beyond what comes with the image, you'll likely want to specify one of these explicitly to minimize breakage when there are new releases of Debian.

These images are built off the buildpack-deps image. It, by design, has a large number of extremely common Debian packages.

These images contain the Quicklisp installer, located at `/usr/local/share/common-lisp/source/quicklisp/quicklisp.lisp`. Additionally, there is a script at `/usr/local/bin/install-quicklisp` that will use the bundled installer to install Quicklisp. You can configure the Quicklisp install with the following environment variables:

-   **`QUICKLISP_DIST_VERSION`:** The dist version to use. Of the form yyyy-mm-dd. `latest` means to install the latest version (the default).
-   **`QUICKLISP_CLIENT_VERSION`:** The client version to use. Of the form yyyy-mm-dd. `latest` means to install the latest version (the default).
-   **`QUICKLISP_ADD_TO_INIT_FILE`:** If set to `true`, `(ql:add-to-init-file)` is used to add code to the implementation's user init file to load Quicklisp on startup. Not set by default.

Additionally, these images contain cl-launch to provide a uniform interface to running a Lisp implementation without caring exactly which implementation is being used (for instance to have uniform CI scripts).


<a id="orgb7d492c"></a>

## `%%IMAGE%%:<version>-slim`

This image does not contain the common packages contained in the default tag and only contains the minimal packages needed to run CMUCL. Unless you are working in an environment where only this image will be deployed and you have space constraints, we highly recommend using the default image of this repository.


<a id="org4cb38f4"></a>

# License

CMUCL is freely redistributable: free, with full source code (most of which is in the public domain) and no strings attached (and no warranty).

The Dockerfiles used to build the images are licensed under BSD-2-Clause.

As with all Docker images, these likely also contain other software which may be under other licenses (such as Bash, etc from the base distribution, along with any direct or indirect dependencies of the primary software being contained).

As for any pre-built image usage, it is the image user's responsibility to ensure that any use of this image complies with any relevant licenses for all software contained within.
