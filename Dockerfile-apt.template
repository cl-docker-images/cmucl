# This image installs the binary becuase the latest release has some issues
# with modern gcc.

FROM PLACEHOLDER

# hadolint ignore=DL3008
RUN set -x \
    && apt-get update \
    && apt-get install --no-install-recommends -y cl-launch \
    && rm -rf /var/lib/apt/lists/*

ENV CMUCL_VERSION PLACEHOLDER
ENV CMUCL_SIGNING_KEY 0EF50ED55514BFF6B72B9DAC06CE3819086C750B

WORKDIR /usr/local/src/

RUN set -x \
    && apt-get update \
    && apt-get install -y --no-install-recommends libc6-dev-i386 gcc-multilib \
    && GNUPGHOME="$(mktemp -d)" \
    && export GNUPGHOME \
    && gpg --batch --keyserver keyserver.ubuntu.com --recv-keys ${CMUCL_SIGNING_KEY} \
    && curl -fsSL "https://cmucl.org/downloads/release/$CMUCL_VERSION/cmucl-$CMUCL_VERSION-x86-linux.tar.bz2" > "cmucl-$CMUCL_VERSION-x86-linux.tar.bz2" \
    && curl -fsSL "https://cmucl.org/downloads/release/$CMUCL_VERSION/cmucl-$CMUCL_VERSION-x86-linux.tar.bz2.asc" > "cmucl-$CMUCL_VERSION-x86-linux.tar.bz2.asc" \
    && curl -fsSL "https://cmucl.org/downloads/release/$CMUCL_VERSION/cmucl-$CMUCL_VERSION-x86-linux.extra.tar.bz2" > "cmucl-$CMUCL_VERSION-x86-linux.extra.tar.bz2" \
    && curl -fsSL "https://cmucl.org/downloads/release/$CMUCL_VERSION/cmucl-$CMUCL_VERSION-x86-linux.extra.tar.bz2.asc" > "cmucl-$CMUCL_VERSION-x86-linux.extra.tar.bz2.asc" \
    && gpg --batch --verify "cmucl-$CMUCL_VERSION-x86-linux.tar.bz2.asc" "cmucl-$CMUCL_VERSION-x86-linux.tar.bz2" \
    && gpg --batch --verify "cmucl-$CMUCL_VERSION-x86-linux.extra.tar.bz2.asc" "cmucl-$CMUCL_VERSION-x86-linux.extra.tar.bz2" \
    && tar -C /usr/local -xvf "cmucl-$CMUCL_VERSION-x86-linux.tar.bz2" \
    && tar -C /usr/local -xvf "cmucl-$CMUCL_VERSION-x86-linux.extra.tar.bz2" \
    && mv /usr/local/bin/lisp /usr/local/bin/cmucl \
    && rm "cmucl-$CMUCL_VERSION-x86-linux.tar.bz2" "cmucl-$CMUCL_VERSION-x86-linux.tar.bz2.asc" "cmucl-$CMUCL_VERSION-x86-linux.extra.tar.bz2" "cmucl-$CMUCL_VERSION-x86-linux.extra.tar.bz2.asc" \
    && rm -rf /var/lib/apt/lists/* \
    && cmucl -eval '(print (lisp-implementation-version))' -eval '(quit)'

# Add the Quicklisp installer.
WORKDIR /usr/local/share/common-lisp/source/quicklisp/

ENV QUICKLISP_SIGNING_KEY D7A3489DDEFE32B7D0E7CC61307965AB028B5FF7

RUN set -x \
    && curl -fsSL "https://beta.quicklisp.org/quicklisp.lisp" > quicklisp.lisp \
    && curl -fsSL "https://beta.quicklisp.org/quicklisp.lisp.asc" > quicklisp.lisp.asc \
    && GNUPGHOME="$(mktemp -d)" \
    && export GNUPGHOME \
    && gpg --batch --keyserver keyserver.ubuntu.com --recv-keys "${QUICKLISP_SIGNING_KEY}" \
    && gpg --batch --verify "quicklisp.lisp.asc" "quicklisp.lisp" \
    && rm quicklisp.lisp.asc \
    && rm -rf "$GNUPGHOME"

# Add the script to trivially install Quicklisp
COPY install-quicklisp /usr/local/bin/install-quicklisp

# Add the entrypoint
WORKDIR /

COPY docker-entrypoint.sh /usr/local/bin/docker-entrypoint.sh

ENTRYPOINT ["docker-entrypoint.sh"]

CMD ["cmucl"]
