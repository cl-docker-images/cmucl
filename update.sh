#!/usr/bin/env bash
set -Eeuo pipefail

cd "$(dirname "$(readlink -f "$BASH_SOURCE")")"

versions=( "$@" )

generated_warning() {
    cat <<EOH
#
# NOTE: THIS DOCKERFILE IS GENERATED VIA "update.sh"
#
# PLEASE DO NOT EDIT IT DIRECTLY.
#
EOH
}

for version in "${versions[@]}"; do

    if [ "$version" = "nightly" ]; then
        echo "No nightly support yet(?)" >&2
        exit 1
    fi

    for v in \
        bullseye/{,slim} \
        buster/{,slim} \
    ; do
        os="${v%%/*}"
        variant="${v#*/}"
        dir="$version/$v"

        mkdir -p "$dir"

        case "$os" in
            bullseye|buster|stretch)
                template="apt"
                if [ "$variant" = "slim" ]; then
                    from="debian:$os"
                else
                    from="buildpack-deps:$os"
                    cp install-quicklisp "$dir/install-quicklisp"
                fi
                cp docker-entrypoint.sh "$dir/docker-entrypoint.sh"
                ;;
        esac

        if [ -n "$variant" ]; then
            template="$template-$variant"
        fi

        if [ "$version" = "nightly" ]; then
            template="$template-nightly"
        fi

        template="Dockerfile-${template}.template"

        { generated_warning; cat "$template"; } > "$dir/Dockerfile"

        sed -ri \
            -e 's/^(ENV CMUCL_VERSION) .*/\1 '"$version"'/' \
            -e 's,^(FROM) .*,\1 '"$from"',' \
            "$dir/Dockerfile"
    done
done
